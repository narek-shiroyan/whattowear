export const makeClimate = (temperature) => {
  let climate = null;
  if (temperature <= -20) {
    climate = 'VeryCold';
  }
  if (temperature > -20 && temperature < 15) {
    climate = 'Cold';
  }
  if (temperature >= 15 && temperature < 20) {
    climate = 'Warm';
  }
  if (temperature >= 20) {
    climate = 'Hot';
  }

  return climate;
}