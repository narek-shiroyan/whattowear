export const weatherImages = {
  '01d': require('../../assets/weather/01d.png'),
  '02d': require('../../assets/weather/02d.png'),
  '03d': require('../../assets/weather/03d.png'),
  '04d': require('../../assets/weather/04d.png'),
  '09d': require('../../assets/weather/09d.png'),
  '10d': require('../../assets/weather/10d.png'),
  '11d': require('../../assets/weather/11d.png'),
  '13d': require('../../assets/weather/13d.png'),
  '50d': require('../../assets/weather/04n.png'),
  '01n': require('../../assets/weather/01n.png'),
  '02n': require('../../assets/weather/02n.png'),
  '03n': require('../../assets/weather/03n.png'),
  '04n': require('../../assets/weather/04n.png'),
  '09n': require('../../assets/weather/09n.png'),
  '10n': require('../../assets/weather/10n.png'),
  '11n': require('../../assets/weather/11n.png'),
  '13n': require('../../assets/weather/13n.png'),
  '50n': require('../../assets/weather/50n.png'),
};

export const clothesImages = {
  VeryCold: {
    m: {
      hat: require('../../assets/clothes/VeryCold/m/hat.png'),
      body: require('../../assets/clothes/VeryCold/m/body.png'),
      legs: require('../../assets/clothes/VeryCold/m/legs.png'),
      bots: require('../../assets/clothes/VeryCold/m/bots.png'),
    },
    w: {
      hat: require('../../assets/clothes/VeryCold/w/hat.png'),
      body: require('../../assets/clothes/VeryCold/w/body.png'),
      legs: require('../../assets/clothes/VeryCold/w/legs.png'),
      bots: require('../../assets/clothes/VeryCold/w/bots.png'),
    },
  },
  Cold: {
    m: {
      body: require('../../assets/clothes/Cold/m/body.png'),
      legs: require('../../assets/clothes/Cold/m/legs.png'),
      bots: require('../../assets/clothes/Cold/m/bots.png'),
    },
    w: {
      body: require('../../assets/clothes/Cold/w/body.png'),
      bots: require('../../assets/clothes/Cold/w/bots.png'),
    },
  },
  Warm: {
    m: {
      body: require('../../assets/clothes/Warm/m/body.png'),
      legs: require('../../assets/clothes/Warm/m/legs.png'),
      bots: require('../../assets/clothes/Warm/m/bots.png'),
    },
    w: {
      body: require('../../assets/clothes/Warm/w/body.png'),
      legs: require('../../assets/clothes/Warm/w/legs.png'),
      bots: require('../../assets/clothes/Warm/w/bots.png'),
    },
  },
  Hot: {
    m: {
      body: require('../../assets/clothes/Hot/m/body.png'),
      legs: require('../../assets/clothes/Hot/m/legs.png'),
      bots: require('../../assets/clothes/Hot/m/bots.png'),
    },
    w: {
      body: require('../../assets/clothes/Hot/w/body.png'),
      bots: require('../../assets/clothes/Hot/w/bots.png'),
    },
  },
};
