import GetLocation from 'react-native-get-location';
import Geocoder from 'react-native-geocoder';

/**
 * Функция выдачи текущик координат
 * @return {lat, lon}
 */
export const getCurrentPosition = async () => {
  try {
    const position = await GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    });
    return position;
  } catch (e) {
    console.warn(e);
  }
};

/**
 * Функция получения города по координатам
 * @param - {String} - lat
 * @param - {String} - lng
 * @return - {String} - city
 */
export const getCurrentCity = async (lat, lng) => {
  try {
    const position = await Geocoder.geocodePosition({ lat, lng });
    return position[0].locality;
  } catch (e) {
    console.warn(e);
  }
};

