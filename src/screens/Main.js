import { getWeather } from 'api/weather';
import ButtonsGroup from 'components/Blocks/ButtonsGroup';
import CityBlock from 'components/Blocks/CityBlock';
import ClothesBlock from 'components/Blocks/ClothesBlock';
import InfoWeather from 'components/Blocks/InfoWeather';
import WeatherBlock from 'components/Blocks/WeatherBlock';
import { Container, Content } from 'native-base';
import React from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
} from 'react-native';
import { getCurrentCity, getCurrentPosition } from 'utils/geolocation';
import { makeClimate } from 'utils/weather';

const MainScreen = (props) => {

  const [mainLoading, setMainLoading] = React.useState(true);
  const [weather, setWeather] = React.useState({});
  const [city, setCity] = React.useState('');
  const [period, setPeriod] = React.useState('current'); // Период времени (день, ночь, утро и тд.)
  const [weatherStatus, setWeatherStatus] = React.useState(null); // Статус погода (облачно, ясно и тд.)
  const [temperature, setTemperature] = React.useState(null); // Текущая температура
  const [feelsLike, setFellsLike] = React.useState(null); // Ощущается как
  const [climate, setClimate] = React.useState(null); // Состояние погоды (холодно, жарко и тд.)

  React.useEffect(() => {
    // Ассинхронная функция
    loadMain();
  }, []);

  // Заполнение температуры
  const image = require('../../assets/bg/day.jpg');
  if (mainLoading) {
    return <ActivityIndicator />;
  }
  console.log('WEATHER', weather);

  return (
    <Container>
      <ImageBackground source={image} style={styles.image}>
        {/* <Header transparent /> */}
        <Content>
          <ScrollView>
            <CityBlock city={city} />
            <WeatherBlock
              status={weatherStatus}
              temp={temperature}
              feelsLike={feelsLike}
              period={period}
            />
            <ClothesBlock climate={climate} />
            <InfoWeather />
            <ButtonsGroup callBack={buttonGroupPress} period={period} />
          </ScrollView>
        </Content>
      </ImageBackground>
    </Container>
  );

  async function loadMain() {
    console.log('LOAD_MAIN');
    // Получить текущие координаты
    const { latitude, longitude } = await getCurrentPosition();
    console.log(latitude, longitude);
    // Получить название города
    //55.384205, 36.722906 Временно другие координаты
    const currentCity = await getCurrentCity(latitude, longitude);

    setCity(currentCity);
    const loadedWeather = await getWeather(latitude, longitude);
    await setWeather(loadedWeather);
    setWeatherStatus(loadedWeather.current.weather[0]);
    // Заполнение температуры
    await setTemperature(Math.floor(loadedWeather.current.temp));
    setFellsLike(Math.floor(loadedWeather.current.feels_like));
    changeTemperature(period);
    // console.log('из changeTemperature', loadedWeather.current.feels_like, makeClimate(loadedWeather.current.feels_like));
    setMainLoading(false);
  }

  /**  
   * Изменение периода дня
   * @param {string} element
   */
  function buttonGroupPress(element) {
    changeTemperature(element);
    return setPeriod(element);
  }

  /**
   * Изменение периода
   * @param {String} c
   */
  function changeTemperature(c) {
    console.log('changeTemperature');
    if (c === 'current') {
      const currentTemp = weather.current.temp;
      const currentFeelsLike = weather.current.feels_like;

      setTemperature(Math.floor(currentTemp)); // Заполняем температуру
      setFellsLike(Math.floor(currentFeelsLike)); // Заполняем ощущается как
      setClimate(makeClimate(currentFeelsLike)); // Создаем статус погоды (тепло, холодно и тд)
    }
    if (c === 'day') {
      const dayTemp = weather.daily[0].temp.day;
      const dayFeelsLike = weather.daily[0].feels_like.day;

      setTemperature(Math.floor(dayTemp));
      setFellsLike(Math.floor(dayFeelsLike));
      setClimate(makeClimate(dayFeelsLike));
    }
    if (c === 'evening') {
      const eveTemp = weather.daily[0].temp.eve;
      const eveFeelsLike = weather.daily[0].feels_like.eve;

      setTemperature(Math.floor(eveTemp));
      setFellsLike(Math.floor(eveFeelsLike));
      setClimate(makeClimate(eveFeelsLike));

    }
    if (c === 'morning') {
      const morningTemp = weather.daily[0].temp.morn;
      const morningFeelsLike = weather.daily[0].feels_like.morn;

      setTemperature(Math.floor(morningTemp));
      setFellsLike(Math.floor(morningFeelsLike));
      setClimate(makeClimate(morningFeelsLike));

    }
    if (c === 'night') {
      const nightTemp = weather.daily[0].temp.night;
      const nightFeelsLike = weather.daily[0].feels_like.night;

      setTemperature(Math.floor(nightTemp));
      setFellsLike(Math.floor(nightFeelsLike));
      setClimate(makeClimate(nightFeelsLike));

    }
  }
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    // backgroundColor: '#faf',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    paddingLeft: '5%',
    paddingRight: '5%',
    // justifyContent: 'center',
  },
});

export default MainScreen;

// api.openweathermap.org/data/2.5/weather?q=moscow&appid=d3032c57ae11bea906669d10ac8966c6
