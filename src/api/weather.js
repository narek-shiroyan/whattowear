import axios from 'axios';
import { WEATHER_API_KEY, WEATHER_API_URL } from 'react-native-dotenv';

//http://api.openweathermap.org/data/2.5/weather?lat=37.33445442&lon=-122.04458579&appid=d3032c57ae11bea906669d10ac8966c6&lang=ru&units=metric
// Пример запроса для получения данных
// http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={key}&lang=ru&units=metric

// https://samples.openweathermap.org/data/2.5/forecast/daily?id=524901&lang=zh_cn&appid=439d4b804bc8187953eb36d2a8c26a02
// https://api.openweathermap.org/data/2.5/onecall?lat=33.441792&lon=-94.037689&exclude=hourly&appid=d3032c57ae11bea906669d10ac8966c6
// https://api.openweathermap.org/data/2.5/onecall?lat=33.441792&lon=-94.037689&exclude=hourly&appid=d3032c57ae11bea906669d10ac8966c6
// http://api.openweathermap.org/data/2.5/weather?q=naro-fominsk&appid=d3032c57ae11bea906669d10ac8966c6&lang=ru&units=metric
export const getWeather = (lat, lon) => {
  if (!lat || !lon) {
    return;
  }

  return axios
    .get(
      //`${WEATHER_API_URL}/weather?q=${city}&appid=${WEATHER_API_KEY}&lang=ru&units=metric`,
      `${WEATHER_API_URL}/onecall?lat=${lat}&lon=${lon}&appid=${WEATHER_API_KEY}&lang=ru&units=metric`,
    )
    .then((response) => {
      return response.data;
    });
};
