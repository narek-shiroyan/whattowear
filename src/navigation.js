import { Button, Icon } from 'native-base';
import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainScreen from 'screens/Main';
import SettingsScreen from 'screens/Settings';

const AppNavigator = createStackNavigator(
  {
    Main: {
      screen: MainScreen,
    },
    Settings: {
      screen: SettingsScreen,
    },
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      // headerTransparent: true,
      // headerTitle: false,
      // headerShown: екгу,
      headerRight: () => (
        <Button transparent onPress={() => alert('Настройки')}>
          <Icon name="settings-outline" style={styles.iconButton} />
        </Button>),
    },
  },
);

const styles = StyleSheet.create({
  iconButton: {
    color: Colors.darker,
  },
});
export default createAppContainer(AppNavigator);
