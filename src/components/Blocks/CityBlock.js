import { Button, Icon } from 'native-base';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const CityBlock = (props) => {
  const { city } = props;
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.city}>{city}</Text>
      </View>
      <View style={styles.buttonView}>
        <Button transparent onPress={() => alert('Настройки')}>
          <Icon name="settings-outline" style={styles.iconButton} />
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  city: {
    fontSize: 30,
    color: Colors.darker,
  },
  buttonView: {
    marginLeft: 'auto',
  },
  iconButton: {
    color: Colors.darker,
  },
});

export default CityBlock;
