import { Thumbnail } from 'native-base';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { weatherImages } from 'utils/imageRequires';

const WeatherBlock = (props) => {
  const {
    status: { icon, description, main },
    temp,
    feelsLike,
    period,
  } = props;
  const image = weatherImages[icon];
  return (
    <View>
      <View style={styles.iconView}>
        <Thumbnail source={image} style={styles.iconItem} />
        <Text style={styles.infoItem}>{description}</Text>
      </View>
      <View>
        <Text style={styles.infoItem}>
          {periodTranslate[period]} {temp} ℃
        </Text>
        <Text style={styles.infoItem}>Ощущается как {feelsLike} ℃</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconView: {
    flexDirection: 'row',
  },
  iconItem: {
    width: 50,
    height: 50,
  },
  infoItem: {
    color: Colors.white,
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 10,
  },
});

const periodTranslate = {
  current: 'сейчас',
  day: 'днем',
  evening: 'вечером',
  morning: 'утром',
  night: 'ночью',
};

export default WeatherBlock;
