import { Button } from 'native-base';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const ButtonsGroup = (props) => {
  const { callBack, period } = props;
  console.log('PERIOD', period);
  return (
    <View style={styles.container}>
      {buttons.map((el, key) => (
        <Button
          style={styles.buttonView}
          key={key}
          transparent={period === el.id}
          onPress={() => callBack(el.id)}>
          <Text style={styles.text}>{el.name}</Text>
        </Button>
      ))}
    </View>
  );
};

const buttons = [
  {
    id: 'current',
    name: 'Сейчас',
  },
  {
    id: 'morning',
    name: 'Утром',
  },
  {
    id: 'day',
    name: 'Днем',
  },
  {
    id: 'evening',
    name: 'Вечером',
  },
];

export default ButtonsGroup;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonView: {
    margin: 10,
  },
  text: {
    paddingLeft: 10,
    paddingRight: 10,
    color: Colors.white,
  },
});
