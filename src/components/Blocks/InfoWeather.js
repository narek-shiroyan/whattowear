import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const InfoWeather = (props) => {
  return (
    <View style={styles.infoView}>
      <Text style={styles.infoViewText}>Одевайтесь потеплее</Text>
    </View>
  );
};

export default InfoWeather;

const styles = StyleSheet.create({
  infoView: {},
  infoViewText: {
    color: Colors.white,
    fontSize: 20,
    textAlign: 'center',
  },
});
