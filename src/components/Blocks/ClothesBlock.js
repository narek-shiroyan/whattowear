import React from 'react';
import { Dimensions, Image, StyleSheet } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { clothesImages } from 'utils/imageRequires';

const deviceHeight = Dimensions.get('screen').height;

const ClothesBlock = (props) => {
  const { climate } = props;
  const sex = 'm';
  const hat = clothesImages[climate].[sex].hat;
  const body = clothesImages[climate].[sex].body;
  const legs = clothesImages[climate].[sex].legs;
  const bots = clothesImages[climate].[sex].bots;

  return (
    <Grid style={styles.container}>
      <Col style={[styles.col, styles.leftCol]}>
        <Row size={1} style={styles.row}>
          <Image source={hat} resizeMode="center" style={styles.images} />
        </Row>
        <Row size={3} style={styles.row}>
          <Image source={legs} resizeMode="contain" style={styles.images} />
        </Row>
      </Col>
      <Col style={[styles.col, styles.rightCol]}>
        <Row size={3} style={styles.row}>
          <Image source={body} resizeMode="contain" style={styles.images} />
        </Row>
        <Row size={2} style={styles.row}>
          <Image source={bots} resizeMode="contain" style={styles.images} />
        </Row>
      </Col>
    </Grid>
  );
};

const styles = StyleSheet.create({
  container: {
    height: deviceHeight / 2.2,
    // backgroundColor: '#f22',
    marginTop: 10,
  },
  row: {
    margin: 5,
    // backgroundColor: '#f2f',
    textAlign: 'left',
  },
  col: {
    // height: '60%',
  },
  leftCol: {
    marginRight: -50,
  },
  rightCol: {
    marginLeft: -50,
  },
  images: {
    width: '100%',
    height: '100%',
  },
});

// hat, body, legs, bots

export default ClothesBlock;
